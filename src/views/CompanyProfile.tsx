import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";
import AppBar from "@material-ui/core/AppBar";
import Typography from '@material-ui/core/Typography';
import {useHistory} from 'react-router-dom';
import {Button, ButtonGroup, Divider, IconButton} from "@material-ui/core";
import företagsBild from "../img/företagsBild.jpeg";
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import SearchIcon from '@material-ui/icons/Search';
import FavoriteIcon from '@material-ui/icons/Favorite';
import StarsIcon from "@material-ui/icons/Stars";

export default function CompanyProfile() {
    const classes = useStyles();
    const history = useHistory();

    let futureFetchedCompanyName: string = "Företagsnamn AB";
    let futureFetchedDiscription: string =
        "Vi på företagsnamn AB jobbar med de nyaste teknikerna och har alltid spännande uppdrag.\n" +
        "                Vi har många stora kunder i över hela Sverige och expanderar ständigt. Vi vill satsa\n" +
        "                på nya talanger och tar därför in ett par LIA-praktikanter till höst."

    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <Typography variant="body1" color="inherit" align="left" className={classes.appBar}>
                        Mina sidor
                    </Typography>
                    <IconButton onClick={() => history.push("/")}>
                        <HomeIcon className={classes.icon}/>
                    </IconButton>
                        <AccountBoxIcon className={classes.icon}/>
                    <IconButton onClick={() => history.push("/CompanyProfile/Sök")}>
                        <SearchIcon className={classes.icon}/>
                    </IconButton>
                    <IconButton onClick={() => history.push("/CompanyProfile/Favoriter")}>
                        <StarsIcon className={classes.icon}/>
                    </IconButton>

                </Toolbar>
            </AppBar>
            <Typography variant={"h6"} className={classes.companyHeadline}>{futureFetchedCompanyName}</Typography>
            <Divider style={{margin: 20}}/>
            <img src={företagsBild} alt="" className={classes.img}/>
            <Typography variant={"body2"} className={classes.descriptionText}>{futureFetchedDiscription}</Typography>
            <Divider style={{margin: 20}}/>
            <ButtonGroup
                orientation="vertical"
                color="primary"
                aria-label="vertical contained primary button group"
                variant="text"
            >
                <Button className={classes.button} onClick={() => history.push("/CompanyProfile/Sök")}>
                    Sök studenter</Button>
                <Button onClick={() => history.push("/CompanyProfile/Favoriter")}>
                    Visa Favoriter</Button>
                <Button onClick={() => history.push("/StudentProfil/redigera")}>
                   Redigera profil</Button>
            </ButtonGroup>
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    icon: {
        margin: 5
    },
    appBar: {
        width: "100%",
    },
    img: {
        height: 200,
        marginLeft: 20,
        marginRight: 20
    },
    companyHeadline: {
        marginTop: 20,
        marginBottom: 20
    },
    descriptionText: {
        margin: 'auto',
        maxWidth: 500,
        padding: 20
    },
    button: {
        marginTop: 20,
    }
}));