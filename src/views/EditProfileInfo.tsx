import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";
import AppBar from "@material-ui/core/AppBar";
import Typography from '@material-ui/core/Typography';
import {useHistory} from 'react-router-dom';
import {Box, Button, ButtonGroup, Divider, IconButton, TextField} from "@material-ui/core";
import företagsBild from "../img/företagsBild.jpeg";
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import SearchIcon from '@material-ui/icons/Search';
import FavoriteIcon from '@material-ui/icons/Favorite';
import StarsIcon from "@material-ui/icons/Stars";


export default function EditProfileInfo() {
    const history = useHistory();
    const classes = useStyles();

    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <Typography variant="body1" color="inherit" align="left" className={classes.appBar}>
                        Mina sidor
                    </Typography>
                    <IconButton onClick={() => history.push("/")}>
                        <HomeIcon className={classes.icon}/>
                    </IconButton>
                    <AccountBoxIcon className={classes.icon}/>
                    <IconButton onClick={() => history.push("/CompanyProfile/Sök")}>
                        <SearchIcon className={classes.icon}/>
                    </IconButton>
                    <IconButton onClick={() => history.push("/CompanyProfile/Favoriter")}>
                        <StarsIcon className={classes.icon}/>
                    </IconButton>

                </Toolbar>
            </AppBar>
      <Typography variant="h5" style={{margin:10}}>Redigera profil</Typography>
            <div className={classes.form}>
            <TextField
                id="outlined-multiline-static"
                label="Beskrivning"
                multiline
                rows={4}
                defaultValue=""
            />
            </div>
            <Button style={{margin:10}} variant="contained" color="primary">Spara</Button>
        </div>
    );
}
const useStyles = makeStyles((theme) => ({
    icon: {
        margin: 5
    },
    appBar: {
        width: "100%",
    },
    img: {
        height: 200,
        marginLeft: 20,
        marginRight: 20
    },
    form: {
        borderStyle: 'solid',
        borderWidth: 1,
        maxWidth:200,
        padding: 20,
        margin: 'auto',
        marginTop: 20,
    }
}));