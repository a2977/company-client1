import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {IconButton} from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import SearchIcon from "@material-ui/icons/Search";
import {useHistory} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import StarsIcon from '@material-ui/icons/Stars';
import ImgList from "./components/ImgList";

interface student {
    name: string;
    course: string;
    school: string;
    city: string;
    img: string;
}
const students: student[] = [
    {name: "Rihanna", course: "ingenjör", school: "ec-utbildning", city: "Stockholm", img: "https://images.jpost.com/image/upload/f_auto,fl_lossy/t_JD_ArticleMainImageFaceDetect/476440"},
    {name: "Markoolio", course: "javautvecklare", school: "ec-utbildning", city: "Stockholm", img: "https://www.vemdalenidag.nu/upload/news/64046116.jpg"},
    {name: "Björn Gustavsson", course: "lärare", school: "nån yrkeshögskola", city: "Göteborg", img: "https://images.aftonbladet-cdn.se/v2/images/b3382e9d-c673-48f6-a5fb-9ef608fbc29e?fit=crop&format=auto&h=272&q=50&w=440&s=65a439bc2638c96293d1150a88389fb5244c19f3"},
];

export default function Favorites() {
    const classes = useStyles();
    const history = useHistory();

    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <Typography variant="body1" color="inherit" align="left" className={classes.appBar}>
                        Favoriter
                    </Typography>
                    <IconButton onClick={() => history.push("/")}>
                        <HomeIcon className={classes.icon}/>
                    </IconButton>
                    <IconButton onClick={() => history.push("/CompanyProfile")}>
                    <AccountBoxIcon className={classes.icon}/>
                    </IconButton>
                    <IconButton onClick={() => history.push("/CompanyProfile/Sök")}>
                        <SearchIcon className={classes.icon}/>
                    </IconButton>
                        <StarsIcon className={classes.icon}/>
                </Toolbar>
            </AppBar>
         <ImgList student={students}/>
        </div>
    )
}
const useStyles = makeStyles((theme) => ({
    icon: {
        margin: 5
    },
    appBar: {
        width: "100%",
    }
}));