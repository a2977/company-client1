import React from 'react';
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import {makeStyles} from "@material-ui/core/styles";
import {useHistory} from "react-router-dom";
import {IconButton} from "@material-ui/core";
import SearchBarAndList from "./components/SearchBarAndList";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import SearchIcon from "@material-ui/icons/Search";
import StarsIcon from "@material-ui/icons/Stars";

export default function SearchView() {
    const classes = useStyles();
    const history = useHistory();

    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <Typography variant="body1" color="inherit" align="left" className={classes.appBar}>
                        Sök
                    </Typography>
                    <IconButton onClick={() => history.push("/")}>
                        <HomeIcon className={classes.icon}/>
                    </IconButton>
                    <IconButton onClick={() => history.push("/CompanyProfile")}>
                    <AccountBoxIcon className={classes.icon}/>
                    </IconButton>
                        <SearchIcon className={classes.icon}/>
                    <IconButton onClick={() => history.push("/CompanyProfile/Favoriter")}>
                        <StarsIcon className={classes.icon}/>
                    </IconButton>
                </Toolbar>
            </AppBar>
           <SearchBarAndList/>
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    icon: {
        margin: 5
    },
    appBar: {
        width: "100%",
    },
}));