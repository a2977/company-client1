import React from 'react';
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import {makeStyles} from "@material-ui/core/styles";
import {Link, useHistory} from "react-router-dom";
import {Button, IconButton} from "@material-ui/core";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import SearchIcon from "@material-ui/icons/Search";
import StarsIcon from "@material-ui/icons/Stars";

export default function StartPage() {
    const classes = useStyles();
    const history = useHistory();
    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <Typography variant="body1" color="inherit" align="left" style={{width: "100%"}}>
                        Företag
                    </Typography>
                    <HomeIcon className={classes.icon}/>
                    <IconButton onClick={() => history.push("/CompanyProfile")}>
                        <AccountBoxIcon className={classes.icon}/>
                    </IconButton>
                    <IconButton onClick={() => history.push("/CompanyProfile/Sök")}>
                        <SearchIcon className={classes.icon}/>
                    </IconButton>
                    <IconButton onClick={() => history.push("/CompanyProfile/Favoriter")}>
                        <StarsIcon className={classes.icon}/>
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Button className={classes.button} variant="contained" color="primary"
                    onClick={() => history.push("/CompanyProfile")}>
                Gå till mina sidor
            </Button>
        </div>
    )
}
const useStyles = makeStyles((theme) => ({
    icon: {
        margin: 5
    },
    button: {
        marginTop: 50,
    }
}));