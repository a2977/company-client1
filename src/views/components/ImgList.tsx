import React, {FC} from 'react';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import ImageListItemBar from '@material-ui/core/ImageListItemBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import {Button, Typography} from "@material-ui/core";
import {useHistory} from "react-router-dom";

interface Student {
    name: string,
    course: string,
    school: string,
    city: string,
    img: string
}

interface StudentProps {
    student: Student[],
}

const ImgList: FC<StudentProps> = ({student}): JSX.Element=> {
    const classes = useStyles();
    const history = useHistory();

    return (
        <div className={classes.root}>
            <ImageList rowHeight={180} className={classes.imageList}>
                <ImageListItem key="Subheader" cols={2} style={{ height: 'auto' }}>
                    <Typography variant="h6" style={{marginTop: 20}}>Favoriter</Typography>
                </ImageListItem>
                {student.map((theStudent) => (
                    <ImageListItem key={theStudent.img}>
                        <img src={theStudent.img} alt={theStudent.name}
                             onClick={() => history.push("/StudentProfil/någotId")}/>
                        <ImageListItemBar
                            title={theStudent.name}
                            subtitle={<span>Utbildning: {theStudent.course}</span>}
                            actionIcon={
                                <IconButton aria-label={`info about ${theStudent.name}`} className={classes.icon}>
                                    <InfoIcon />
                                </IconButton>
                            }
                        />
                    </ImageListItem>
                ))}
            </ImageList>
        </div>
    );
}
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-around',
            overflow: 'hidden',
            backgroundColor: theme.palette.background.paper,
        },
        imageList: {
            width: 500,
            height: 450,
        },
        icon: {
            color: 'rgba(255, 255, 255, 0.54)',
        },
    }),
);

export default ImgList