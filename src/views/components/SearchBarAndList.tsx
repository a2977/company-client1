import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import SearchBar from "material-ui-search-bar";
import {useHistory} from "react-router-dom";
import StudentInfoDialog from "./StudentInfoDialog";

interface student {
    name: string;
    course: string;
    school: string;
    city: string;
    img: string;
}

const originalRows: student[] = [
    {name: "Rihanna", course: "ingenjör", school: "ec-utbildning", city: "Stockholm", img: "https://images.jpost.com/image/upload/f_auto,fl_lossy/t_JD_ArticleMainImageFaceDetect/476440"},
    {name: "Markoolio", course: "javautvecklare", school: "ec-utbildning", city: "Stockholm", img: "https://www.vemdalenidag.nu/upload/news/64046116.jpg"},
    {name: "Angelina Jolie", course: "ux design", school: "nån yrkeshögskola", city: "Malmö", img: "https://m.media-amazon.com/images/M/MV5BNTFjZTFmOWQtYjVlNC00MTI2LTk2NTItNDBhMmRjZDY1YWI2XkEyXkFqcGdeQXVyMTEyMjM2NDc2._CR406,453,4440,2498._SY351_SX624_AL_.jpg"},
    {name: "Björn Gustavsson", course: "lärare", school: "nån yrkeshögskola", city: "Göteborg", img: "https://images.aftonbladet-cdn.se/v2/images/b3382e9d-c673-48f6-a5fb-9ef608fbc29e?fit=crop&format=auto&h=272&q=50&w=440&s=65a439bc2638c96293d1150a88389fb5244c19f3"},
];

export default function SearchBarAndList() {
    const [rows, setRows] = useState<student[]>([]);
    const [searched, setSearched] = useState<string>("");
    const classes = useStyles();
    const history = useHistory();

    useEffect(() => {
        fetch("http://localhost:8080/school/students")
            .then(resp => resp.json())
            .then(rows => {setRows(rows)
            
            console.log("rows", rows)
            })
            .catch(err => console.log(err.message))


    },[])

    const cancelSearch = () => {
            fetch("http://localhost:8080/school/students")
                .then(resp => resp.json())
                .then(rows => {setRows(rows)
                    console.log("rows", rows)
                })
                .catch(err => console.log(err.message))
    };

    return (
        <>
            <Paper>
                <SearchBar
                    className={classes.searchBar}
                    value={searched}
                    onChange={(searched) => {
                        fetch(`http://localhost:8080/company/student/name/`+ [searched])
                            .then(res => res.json())
                            .then(apa => {setRows(apa);
                                console.log("apa", apa)
                                console.log("-- backend  searched --", searched)
                            })
                            .catch(err => console.log(err))
                    }}
                    onCancelSearch={() => cancelSearch()}
                />
                <TableContainer>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell style={{fontWeight: 'bold'}}>Namn</TableCell>
                                <TableCell style={{fontWeight: 'bold'}} align="center">Utbildning</TableCell>
                                <TableCell style={{fontWeight: 'bold'}} align="right">Stad</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((student) => (
                                <TableRow key={student.name}>
                                    <StudentInfoDialog student={student}/>
                                    <TableCell align="center">{student.course}</TableCell>
                                    <TableCell align="right">{student.city}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            <br/>
        </>
    );
}

const useStyles = makeStyles({
    table: {
        marginTop: 10,
        paddingLeft: 10,
        paddingRight: 10
    },
    searchBar: {
        margin: 10,
    },
    headLine: {
        font: 'bold',
    }
});
