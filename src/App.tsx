import React from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import CompanyProfile from "./views/CompanyProfile";
import StartPage from "./views/StartPage";
import SearchView from "./views/SearchView";
import Favorites from "./views/Favorites";
import FutureStudentProfile from "./views/FutureStudentProfile";
import EditProfileInfo from "./views/EditProfileInfo";
import { ReactKeycloakProvider } from '@react-keycloak/web'

import keycloak from "./Keykloak"

function App() {

    return (
        <ReactKeycloakProvider authClient={keycloak}>
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/">
                        <StartPage/>
                    </Route>
                    <Route exact path="/CompanyProfile">
                        <CompanyProfile/>
                    </Route>
                    <Route exact path="/CompanyProfile/Sök">
                        <SearchView/>
                    </Route>
                    <Route exact path="/CompanyProfile/Favoriter">
                        <Favorites/>
                    </Route>
                    <Route exact path="/StudentProfil/någotId">
                        <FutureStudentProfile/>
                    </Route>
                    <Route exact path="/StudentProfil/redigera">
                        <EditProfileInfo/>
                    </Route>
                </Switch>
            </Router>
        </div>
        </ReactKeycloakProvider>
    );
}

export default App;
